# Healers in Chief

## Art Contest

Win a prized painting. This artwork could be worth a small fortune one day. To win it, write a short letter describing the message behind it and what impresses you about it. For details, please check in-store.
